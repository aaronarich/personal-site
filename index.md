---
layout: home
---

  My name is Aaron and I’m a front-end developer working and living in Atlanta, Georgia.

  Over the past several years, I’ve had the great pleasure of working on projects with a few agencies, non-profits, and one amazing software company. To learn a bit more about me and what I do, please feel free to take a look around! Thank you for your time.
